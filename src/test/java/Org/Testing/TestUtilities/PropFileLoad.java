package Org.Testing.TestUtilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class PropFileLoad {

	public static Properties propertiesFileLoad(String path) {
		File f = new File(path);
		FileInputStream fi;
		try {
			fi = new FileInputStream(f);
			Properties pr = new Properties();
			pr.load(fi);
			return pr;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
