package Org.Testing.Steps;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

import java.util.Properties;

import Org.Testing.TestUtilities.PropFileLoad;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class PostSearchSteps {

	//private String POST_SEARCH = "https://jsonplaceholder.typicode.com/posts/";
	
	private Response response;
	private Properties prop = PropFileLoad.propertiesFileLoad("Env.properties");
	private String POST_SEARCH = prop.getProperty("URI");
	

	@Step
	public void searchPostByCode(int id) {
		response = SerenityRest.when().get(POST_SEARCH + id);
		System.out.println("The Response: \n\n" + response.toString());
	}

	@Step
	public void searchIsExecutedSuccesfully() {
		response.then().statusCode(200);
	}

	@Step
	public void iShouldFindPostTitle(String title) {
		response.then().body("title", is(title));
	}

	@Step
	public void iShouldNotFindPostTitle(String title) {
		response.then().body("title", is(not(title)));
	}

}
