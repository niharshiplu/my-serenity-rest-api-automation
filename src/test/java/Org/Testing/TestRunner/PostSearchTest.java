package Org.Testing.TestRunner;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import Org.Testing.Steps.*;

@RunWith(SerenityRunner.class)

public class PostSearchTest {
	
	@Steps
	PostSearchSteps postSearchSteps;

	   @Test
	   public void verifyThatWeCanFindEmployeeNameUsingThePostCode1() {
		   postSearchSteps.searchPostByCode(1);
		   postSearchSteps.searchIsExecutedSuccesfully();
		   postSearchSteps.iShouldFindPostTitle("sunt aut facere repellat provident occaecati excepturi optio reprehenderit");
	   }
	   
	   @Test
	   public void verifyThatWeCanFindEmployeeNameUsingThePostCode2() {
		   postSearchSteps.searchPostByCode(2);
		   postSearchSteps.searchIsExecutedSuccesfully();
		   postSearchSteps.iShouldNotFindPostTitle("sunt aut facere repellat provident occaecati excepturi optio reprehenderit"); //Negative Scenario
	   }



}
