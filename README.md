API Documentation:
====================
Route:
/Posts/{id}

Type:
JSON

Http Method:
GET

Full Route:

https://jsonplaceholder.typicode.com/posts/{id}

Description:

Get inforamtion on a particular post data

Sample API Response in JSON format:

{
  "userId": 1,
  "id": 1,
  "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
  "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
}

======================End of API Dcoumentation==============================================================

Guidelines to setup this project and Execute Tests:
===================================================

1. Create a Maven project in your IDE and update the POM file with Required Dependencies for Serenity BDD and Serenity Report.

2. Create seperate package and class for API Endpoints interactions using @Step and to create abstraction with Test Runner Class.

3. Create speperate package and Class for Runner class where new tests can be created and executed.

4.Positie and Negative scenarios are covered as a part of Test Coverage.New Tests can be added here.

5. Use git commands to upload the project in GitLab repository(Public) https://gitlab.com/niharshiplu/my-serenity-rest-api-automation

6. Create .gitlab-ci.yml file in the gitlab to automate the CI of Build,Execution and generation of aggregated HTML report for the project.

7. HTML report has been saved in zip format as a Artifact in the GitLab and can be download.

